import subprocess
import os
import json
import math
import numpy as np
import shutil
from posenetclas.test_utils import move_predict_images
from posenetclas.constants import CONNECTED_PART_NAMES

POINT_NAMES = ["nose", 'leftShoulder', 'rightShoulder', 'leftElbow', 'rightElbow', 'leftWrist', 'rightWrist', 'leftHip',
               'rightHip', 'leftKnee', 'rightKnee', 'leftAnkle', 'rightAnkle']
FRAME_PATH = '/srv/posenet-tf/data/samples/frames'
DEBUG_FILE_PATH = '/srv/posenet-tf/data/samples/results'


def distance(x1, y1, x2, y2):
    dist = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    return dist


def angle(v1, v2):
    dx1 = v1[2] - v1[0]
    dy1 = v1[3] - v1[1]
    dx2 = v2[2] - v2[0]
    dy2 = v2[3] - v2[1]
    angle1 = math.atan2(dy1, dx1)
    angle1 = int(angle1 * 180 / math.pi)
    # print(angle1)
    angle2 = math.atan2(dy2, dx2)
    angle2 = int(angle2 * 180 / math.pi)
    # print(angle2)
    if angle1 * angle2 >= 0:
        included_angle = abs(angle1 - angle2)
    else:
        included_angle = abs(angle1) + abs(angle2)
        if included_angle > 180:
            included_angle = 360 - included_angle
    return included_angle


def body_height_estimate(frame):
    shoulder_point = [(frame['leftShoulder']['x'] + frame['rightShoulder']['x']) / 2.0,
                      (frame['leftShoulder']['y'] + frame['rightShoulder']['y']) / 2.0]
    hip_point = [(frame['leftHip']['x'] + frame['rightHip']['x']) / 2.0,
                 (frame['leftHip']['y'] + frame['rightHip']['y']) / 2.0]
    return distance(shoulder_point[0], shoulder_point[1], hip_point[0], hip_point[1])


def frame_distance(frame_f, frame_b):
    distance_total = 0
    key_distance = {}
    for key in POINT_NAMES:
        score = (frame_f[key]['score'] + frame_b[key]['score']) / 2.0
        if score > 0:
            key_distance[key] = distance(frame_f[key]['x'], frame_f[key]['y'], frame_b[key]['x'], frame_b[key]['y'])
    # print(key_distance)
    shoulder_dis = min(key_distance['leftShoulder'], key_distance['rightShoulder'])
    hip_dis = min(key_distance['leftHip'], key_distance['rightHip'])
    key_distance['leftShoulder'] = key_distance['rightShoulder'] = shoulder_dis
    key_distance['leftHip'] = key_distance['rightHip'] = hip_dis
    for dis in key_distance.values():
        distance_total += dis
    # compute angle
    angle_total = 0
    for (key1, key2) in CONNECTED_PART_NAMES:
        vf = [frame_f[key1]['x'], frame_f[key1]['y'], frame_f[key2]['x'], frame_f[key2]['y']]
        vb = [frame_b[key1]['x'], frame_b[key1]['y'], frame_b[key2]['x'], frame_b[key2]['y']]
        part_angle = angle(vf, vb)
        # print('angle({},{})={}'.format(key1, key2, part_angle))
        angle_total += part_angle
    # print('distance:{} angle:{}'.format(distance_total, angle_total))
    return distance_total / len(POINT_NAMES) * angle_total / len(CONNECTED_PART_NAMES)


def save_pose_to_file(pose_data):
    output_dir = DEBUG_FILE_PATH
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    file_path = os.path.join(output_dir, 'output.json')
    with open(file_path, 'w') as fp:
        json.dump(pose_data, fp)


def movie_to_frames(movie_path, movie_args):
    frame_rate, start_time, duration = movie_args
    if not os.path.exists(FRAME_PATH):
        os.makedirs(FRAME_PATH)
    cmd = 'ffmpeg -i {} {} {} {} -f image2 \'{}/%05d.png\''.format(
        movie_path, '-r ' + str(frame_rate), '-ss ' + start_time,
                    '-t ' + duration, FRAME_PATH)
    subprocess.call(cmd, shell=True)
    images = [file for file in os.listdir(os.path.join('.', FRAME_PATH)) if
              not file.startswith('.')]  # ignore hidden file
    images.sort()
    images = [FRAME_PATH + '/' + img for img in images]
    return images


def remove_frames():
    shutil.rmtree(FRAME_PATH)


def energy_estimation(movie_path, movie_args, body_height=160):
    image_paths = movie_to_frames(movie_path, movie_args)
    # print(image_paths)
    if len(image_paths):
        pose_results = move_predict_images(image_paths, DEBUG_FILE_PATH)
        save_pose_to_file(pose_results)
        key_point_array = pose_results['kp']
        last_key_point = None
        total_distance = 0
        body_height_estimate_array = []
        for key_point in key_point_array:
            if 'nose' not in key_point:
                continue
            if last_key_point is None:
                last_key_point = key_point
                continue
            body_height_estimate_array.append(body_height_estimate(last_key_point))
            total_distance += frame_distance(last_key_point, key_point)
            last_key_point = key_point
        # compute the scale factor
        body_height_estimate_array.sort(reverse=True)
        # print(body_height_estimate_array)
        mean_body_height = np.mean(body_height_estimate_array[:30]) * 3
        print('body_height:{}'.format(mean_body_height))
        scale = body_height / mean_body_height
        energy = total_distance * scale
        remove_frames()
        return energy


energy_result = energy_estimation('/srv/posenet-tf/data/samples/justin.flv', (10, '00:00:00', '00:00:10'), 160)
print('final energy:{}'.format(round(energy_result, 2)))
